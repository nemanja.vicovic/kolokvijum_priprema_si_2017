﻿using DataLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BusinessLayer
{
    public class StudentBusiness
    {
        private StudentRepository studentRepository;

        public StudentBusiness()
        {
            this.studentRepository = new StudentRepository();
        }

        public bool InsertStudent(Student s)
        {
            if(this.studentRepository.InsertStudent(s) > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public List<Student> GetBudgetStudents()
        {
            return this.studentRepository.GetAllStudents().Where(s => s.GetSetIsBudget == true).ToList();
        }

        public List<Student> GetAllStudents()
        {
            return this.studentRepository.GetAllStudents();
        }
    }
}
