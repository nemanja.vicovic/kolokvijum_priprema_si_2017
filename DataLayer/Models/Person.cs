﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer.Models
{
    public class Person
    {
        private int Id;
        private string Name;
        private string Surname;

        public int GetSetId
        {
            get { return Id; }
            set { Id = value; }
        }

        public string GetSetName
        {
            get { return Name; }
            set { Name = value; }
        }

        public string GetSetSurname
        {
            get { return Surname; }
            set { Surname = value; }
        }
    }
}
