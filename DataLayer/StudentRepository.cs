﻿using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataLayer
{
    public class StudentRepository
    {
        public int InsertStudent(Student s)
        {
            using(SqlConnection dataConnection = new SqlConnection(GlobalVariables.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "INSERT INTO Students VALUES('" + s.GetSetName + "', '" + s.GetSetSurname + "', '" + s.GetSetIsBudget + "')";

                return command.ExecuteNonQuery();
            }
        }

        public List<Student> GetAllStudents()
        {
            List<Student> listToReturn = new List<Student>();
            using (SqlConnection dataConnection = new SqlConnection(GlobalVariables.ConnectionString))
            {
                dataConnection.Open();

                SqlCommand command = new SqlCommand();
                command.Connection = dataConnection;
                command.CommandText = "SELECT * FROM Students";

                SqlDataReader dataReader = command.ExecuteReader();

                while (dataReader.Read())
                {
                    Student s = new Student();
                    s.GetSetId = dataReader.GetInt32(0);
                    s.GetSetName = dataReader.GetString(1);
                    s.GetSetSurname = dataReader.GetString(2);
                    s.GetSetIsBudget = dataReader.GetBoolean(3);
                    listToReturn.Add(s);
                }
            }
            return listToReturn;
        }
    }
}
