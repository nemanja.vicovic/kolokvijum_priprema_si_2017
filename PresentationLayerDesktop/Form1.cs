﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace PripremaZaKolokvijum
{
    public partial class Form1 : Form
    {
        private StudentBusiness studentBusiness;

        public Form1()
        {
            InitializeComponent();
            this.studentBusiness = new StudentBusiness();
        }

        private void buttonInsert_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.GetSetName = textBoxName.Text;
            s.GetSetSurname = textBoxSurname.Text;
            s.GetSetIsBudget = checkBoxIsBudget.Checked;
            this.studentBusiness.InsertStudent(s);
            FillStudents();
        }

        private void FillStudents()
        {
            listBoxStudents.Items.Clear();
            List<Student> students = this.studentBusiness.GetAllStudents();

            foreach(Student s in students)
            {
                listBoxStudents.Items.Add(s.GetSetId + ". " + s.GetSetName + " " + s.GetSetSurname);
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            FillStudents();
        }
    }
}
