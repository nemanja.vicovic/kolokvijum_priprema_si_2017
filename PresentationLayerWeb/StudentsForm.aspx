﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="StudentsForm.aspx.cs" Inherits="PresentationLayer.StudentsForm" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
    <br /><br />
    <div class="row">
        <asp:Label ID="Label1" runat="server" Text="Ime: "></asp:Label>
        <asp:TextBox ID="TextBoxName" runat="server"></asp:TextBox>
    </div>
    <br />
    <div class="row">
        <asp:Label ID="Label2" runat="server" Text="Prezime: "></asp:Label>
        <asp:TextBox ID="TextBoxSurname" runat="server"></asp:TextBox>
    </div>
    <br />
    <div class="row">
        <asp:Label ID="Label3" runat="server" Text="Budzet? "></asp:Label>
        <asp:CheckBox ID="CheckBoxIsBudget" runat="server" />
    </div>
    <br />
    <div class="row">
        <asp:Button ID="ButtonInsert" runat="server" Text="Unesi" OnClick="ButtonInsert_Click" />
    </div>
    <br />
    <br />
    <div class="row">
        <asp:Label ID="Label4" runat="server" Text="Svi studenti sa budzeta: "></asp:Label>
    </div>
    <div class="row">
        <asp:ListBox ID="ListBoxStudents" runat="server" Width="100%"></asp:ListBox>
    </div>
</asp:Content>
