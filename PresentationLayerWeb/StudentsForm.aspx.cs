﻿using BusinessLayer;
using DataLayer.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace PresentationLayer
{
    public partial class StudentsForm : System.Web.UI.Page
    {
        private StudentBusiness studentBusiness;

        protected void Page_Load(object sender, EventArgs e)
        {
            this.studentBusiness = new StudentBusiness();
            FillStudents();
        }

        protected void ButtonInsert_Click(object sender, EventArgs e)
        {
            Student s = new Student();
            s.GetSetName = TextBoxName.Text;
            s.GetSetSurname = TextBoxSurname.Text;
            s.GetSetIsBudget = CheckBoxIsBudget.Checked;
            this.studentBusiness.InsertStudent(s);
            FillStudents();
        }

        private void FillStudents()
        {
            ListBoxStudents.Items.Clear();
            List<Student> students = this.studentBusiness.GetBudgetStudents();

            foreach(Student s in students)
            {
                ListBoxStudents.Items.Add(s.GetSetId + ". " + s.GetSetName + " " + s.GetSetSurname);
            }
        }
    }
}