﻿CREATE TABLE [dbo].[Students] (
    [Id]       INT           IDENTITY (1, 1) NOT NULL,
    [Name]     NVARCHAR (50) NOT NULL,
    [Surname]  NVARCHAR (50) NOT NULL,
    [IsBudget] BIT           NULL,
    PRIMARY KEY CLUSTERED ([Id] ASC)
);

